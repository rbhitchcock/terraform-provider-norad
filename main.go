package main

import (
	"github.com/hashicorp/terraform/plugin"
	"gitlab.com/norad/terraform-provider-norad/norad"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: norad.Provider})
}
