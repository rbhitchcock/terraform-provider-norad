[![pipeline status](https://gitlab.com/norad/terraform-provider-norad/badges/master/pipeline.svg)](https://gitlab.com/norad/terraform-provider-norad/commits/master)
[![coverage report](https://gitlab.com/norad/terraform-provider-norad/badges/master/coverage.svg)](https://gitlab.com/norad/terraform-provider-norad/commits/master)

# Norad Terraform Provider

Manage your Norad assets with [Terraform](https://www.terraform.io/)!

## Requirements
* Terraform ~> 0.9
* Go 1.8 (to build the provider plugin)

## Building the Provider

Clone repository to: ```$GOPATH/src/gitlab.com/norad/terraform-provider-norad```

```sh
$ mkdir -p $GOPATH/src/gitlab.com/norad; cd $GOPATH/src/gitlab.com/norad
$ git clone https://gitlab.com/norad/terraform-provider-norad.git
```

Enter the provider directory and build the provider

```sh
$ cd $GOPATH/src/gitlab.com/norad/terraform-provider-norad
$ go install
```

## Using the Provider

TBD

## Developing the Provider

TBD
