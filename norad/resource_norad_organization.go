package norad

import (
	"fmt"
	"github.com/hashicorp/terraform/helper/schema"
	"gitlab.com/norad/go-norad"
)

func resourceNoradOrganization() *schema.Resource {
	return &schema.Resource{
		Create: resourceNoradOrganizationCreate,
		Read:   resourceNoradOrganizationRead,
		Update: resourceNoradOrganizationUpdate,
		Delete: resourceNoradOrganizationDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"uid": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"slug": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"token": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
		},
	}
}

func resourceNoradOrganizationSetToState(d *schema.ResourceData, org *norad.Organization) {
	d.Set("uid", org.Uid)
	d.Set("slug", org.Slug)
	d.Set("token", org.Token)
}

func resourceNoradOrganizationCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	uid := d.Get("uid").(string)
	org_opts := &norad.CreateOrganizationOptions{OrganizationParameters: norad.OrganizationParameters{Uid: uid}}
	new_org, _, err := client.Organizations.CreateOrganization(org_opts)

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", new_org.Id))
	resourceNoradOrganizationSetToState(d, new_org)
	return nil
}

func resourceNoradOrganizationRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	org, _, err := client.Organizations.GetOrganization(d.Id())

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", org.Id))
	resourceNoradOrganizationSetToState(d, org)
	return nil
}

func resourceNoradOrganizationUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)

	update_opts := &norad.UpdateOrganizationOptions{OrganizationParameters: norad.OrganizationParameters{Uid: d.Get("uid").(string)}}

	org, _, err := client.Organizations.UpdateOrganization(d.Id(), update_opts)
	if err != nil {
		return err
	}

	resourceNoradOrganizationSetToState(d, org)
	return nil
}

func resourceNoradOrganizationDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	_, err := client.Organizations.DeleteOrganization(d.Id())
	return err
}
