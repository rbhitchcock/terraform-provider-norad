package norad

import (
	"fmt"
	"github.com/hashicorp/terraform/helper/schema"
	"gitlab.com/norad/go-norad"
)

func resourceNoradGenericService() *schema.Resource {
	return &schema.Resource{
		Create: resourceNoradGenericServiceCreate,
		Read:   resourceNoradGenericServiceRead,
		Update: resourceNoradGenericServiceUpdate,
		Delete: resourceNoradGenericServiceDelete,

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"description": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"port": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"port_type": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"encryption_type": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"allow_brute_force": &schema.Schema{
				Type:     schema.TypeBool,
				Optional: true,
			},
			"machine_id": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
				ForceNew: true,
			},
		},
	}
}

func resourceNoradGenericServiceSetToState(d *schema.ResourceData, generic_service *norad.GenericService) {
	d.Set("description", generic_service.Description)
	d.Set("name", generic_service.Name)
	d.Set("machine_id", generic_service.MachineId)
	d.Set("port", generic_service.Port)
	d.Set("port_type", generic_service.PortType)
	d.Set("encryption_type", generic_service.EncryptionType)
	d.Set("allow_brute_force", generic_service.AllowBruteForce)
}

func resourceNoradGenericServiceCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	mid := d.Get("machine_id").(int)
	generic_service_opts := &norad.CreateGenericServiceOptions{
		GenericServiceCreationParameters: norad.GenericServiceCreationParameters{
			Name:            d.Get("name").(string),
			Description:     d.Get("description").(string),
			Port:            d.Get("port").(string),
			PortType:        d.Get("port_type").(string),
			EncryptionType:  d.Get("encryption_type").(string),
			AllowBruteForce: d.Get("allow_brute_force").(bool),
		},
	}
	generic_service, _, err := client.GenericServices.CreateGenericService(mid, generic_service_opts)

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", generic_service.Id))
	resourceNoradGenericServiceSetToState(d, generic_service)
	return nil
}

func resourceNoradGenericServiceRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	generic_service, _, err := client.GenericServices.GetGenericService(d.Id())

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", generic_service.Id))
	resourceNoradGenericServiceSetToState(d, generic_service)
	return nil
}

func resourceNoradGenericServiceUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)

	options := &norad.UpdateGenericServiceOptions{GenericServiceUpdateParameters: norad.GenericServiceUpdateParameters{}}

	options.GenericServiceUpdateParameters.Description = d.Get("description").(string)
	options.GenericServiceUpdateParameters.Name = d.Get("name").(string)
	options.GenericServiceUpdateParameters.Port = d.Get("port").(string)
	options.GenericServiceUpdateParameters.AllowBruteForce = d.Get("allow_brute_force").(bool)
	options.GenericServiceUpdateParameters.PortType = d.Get("port_type").(string)
	options.GenericServiceUpdateParameters.EncryptionType = d.Get("encryption_type").(string)

	generic_service, _, err := client.GenericServices.UpdateGenericService(d.Id(), options)
	if err != nil {
		return err
	}

	resourceNoradGenericServiceSetToState(d, generic_service)
	return nil
}

func resourceNoradGenericServiceDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	_, err := client.GenericServices.DeleteGenericService(d.Id())
	return err
}
