package norad

import (
	"fmt"
	"github.com/hashicorp/terraform/helper/schema"
	"gitlab.com/norad/go-norad"
)

func resourceNoradMachineSectestConfig() *schema.Resource {
	return &schema.Resource{
		Create: resoruceNoradSecurityTestMachineConfigCreate,
		Read:   resoruceNoradSecurityTestMachineConfigRead,
		Update: resoruceNoradSecurityTestMachineConfigUpdate,
		Delete: resoruceNoradSecurityTestMachineConfigDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"values": &schema.Schema{
				Type:      schema.TypeMap,
				Optional:  true,
				Default:   make(map[string]interface{}),
				Sensitive: true,
			},
			"enabled_outside_of_requirement": &schema.Schema{
				Type:     schema.TypeBool,
				Optional: true,
				Default:  true,
			},
			"sectest_id": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
				ForceNew: true,
			},
			"machine_id": &schema.Schema{
				Type:     schema.TypeInt,
				ForceNew: true,
				Required: true,
			},
		},
	}
}

func resoruceNoradSecurityTestMachineConfigSetToState(d *schema.ResourceData, sectest_config *norad.SecurityTestConfig) {
	d.Set("values", sectest_config.Values)
	d.Set("args", sectest_config.Args)
	d.Set("enabled_outside_of_requirement", sectest_config.EnabledOutsideOfRequirement)
	d.Set("machine_id", sectest_config.MachineId)
	d.Set("sectest_id", sectest_config.SecurityTestId)
}

func resoruceNoradSecurityTestMachineConfigCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	mach_id := d.Get("machine_id").(int)
	sectest_config_opts := &norad.CreateSecurityTestConfigOptions{
		SecurityTestConfigCreationParameters: norad.SecurityTestConfigCreationParameters{
			EnabledOutsideOfRequirement: d.Get("enabled_outside_of_requirement").(bool),
			Values:         d.Get("values").(map[string]interface{}),
			SecurityTestId: d.Get("sectest_id").(int),
		},
	}
	security_test_config, _, err := client.SecurityTestConfigs.CreateMachineSecurityTestConfig(mach_id, sectest_config_opts)

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", security_test_config.Id))
	resoruceNoradSecurityTestMachineConfigSetToState(d, security_test_config)
	return nil
}

func resoruceNoradSecurityTestMachineConfigRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	sectest_config, _, err := client.SecurityTestConfigs.GetSecurityTestConfig(d.Id())

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", sectest_config.Id))
	resoruceNoradSecurityTestMachineConfigSetToState(d, sectest_config)
	return nil
}

func resoruceNoradSecurityTestMachineConfigUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)

	sectest_config_opts := &norad.UpdateSecurityTestConfigOptions{
		SecurityTestConfigUpdateParameters: norad.SecurityTestConfigUpdateParameters{
			EnabledOutsideOfRequirement: d.Get("enabled_outside_of_requirement").(bool),
			Values: d.Get("values").(map[string]interface{}),
		},
	}

	sectest_config, _, err := client.SecurityTestConfigs.UpdateSecurityTestConfig(d.Id(), sectest_config_opts)
	if err != nil {
		return err
	}

	resoruceNoradSecurityTestMachineConfigSetToState(d, sectest_config)
	return nil
}

func resoruceNoradSecurityTestMachineConfigDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	_, err := client.SecurityTestConfigs.DeleteSecurityTestConfig(d.Id())
	return err
}
