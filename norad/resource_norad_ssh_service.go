package norad

import (
	"fmt"
	"github.com/hashicorp/terraform/helper/schema"
	"gitlab.com/norad/go-norad"
)

func resourceNoradSshService() *schema.Resource {
	return &schema.Resource{
		Create: resourceNoradSshServiceCreate,
		Read:   resourceNoradSshServiceRead,
		Update: resourceNoradSshServiceUpdate,
		Delete: resourceNoradSshServiceDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"description": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"port": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"allow_brute_force": &schema.Schema{
				Type:     schema.TypeBool,
				Optional: true,
				Default:  false,
			},
			"machine_id": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
				ForceNew: true,
			},
			"port_type": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"encryption_type": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
		},
	}
}

func resourceNoradSshServiceSetToState(d *schema.ResourceData, ssh_service *norad.SshService) {
	d.Set("description", ssh_service.Description)
	d.Set("name", ssh_service.Name)
	d.Set("machine_id", ssh_service.MachineId)
	d.Set("port", ssh_service.Port)
	d.Set("allow_brute_force", ssh_service.AllowBruteForce)
}

func resourceNoradSshServiceCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	mid := d.Get("machine_id").(int)
	ssh_service_opts := &norad.CreateSshServiceOptions{
		SshServiceCreationParameters: norad.SshServiceCreationParameters{
			Name:            d.Get("name").(string),
			Description:     d.Get("description").(string),
			Port:            d.Get("port").(string),
			AllowBruteForce: d.Get("allow_brute_force").(bool),
		},
	}
	ssh_service, _, err := client.SshServices.CreateSshService(mid, ssh_service_opts)

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", ssh_service.Id))
	resourceNoradSshServiceSetToState(d, ssh_service)
	return nil
}

func resourceNoradSshServiceRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	ssh_service, _, err := client.SshServices.GetSshService(d.Id())

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", ssh_service.Id))
	resourceNoradSshServiceSetToState(d, ssh_service)
	return nil
}

func resourceNoradSshServiceUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)

	options := &norad.UpdateSshServiceOptions{SshServiceUpdateParameters: norad.SshServiceUpdateParameters{}}

	options.SshServiceUpdateParameters.Description = d.Get("description").(string)
	options.SshServiceUpdateParameters.Name = d.Get("name").(string)
	options.SshServiceUpdateParameters.Port = d.Get("port").(string)
	options.SshServiceUpdateParameters.AllowBruteForce = d.Get("allow_brute_force").(bool)

	ssh_service, _, err := client.SshServices.UpdateSshService(d.Id(), options)
	if err != nil {
		return err
	}

	resourceNoradSshServiceSetToState(d, ssh_service)
	return nil
}

func resourceNoradSshServiceDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	_, err := client.SshServices.DeleteSshService(d.Id())
	return err
}
