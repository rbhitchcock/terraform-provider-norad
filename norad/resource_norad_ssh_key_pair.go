package norad

import (
	"fmt"
	"github.com/hashicorp/terraform/helper/schema"
	"gitlab.com/norad/go-norad"
)

func resourceNoradSshKeyPair() *schema.Resource {
	return &schema.Resource{
		Create: resourceNoradSshKeyPairCreate,
		Read:   resourceNoradSshKeyPairRead,
		Update: resourceNoradSshKeyPairUpdate,
		Delete: resourceNoradSshKeyPairDelete,

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"description": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"organization_id": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
				ForceNew: true,
			},
			"username": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				Sensitive:   true,
				Description: "The username associated with the SSH key",
			},
			"key": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				Sensitive:   true,
				Description: "A Base64-encoded SSH private key without a passphrase",
			},
			"key_signature": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
		},
	}
}

func resourceNoradSshKeyPairSetToState(d *schema.ResourceData, ssh_key_pair *norad.SshKeyPair) {
	d.Set("name", ssh_key_pair.Name)
	d.Set("description", ssh_key_pair.Description)
	d.Set("organization_id", ssh_key_pair.OrganizationId)
	d.Set("key_signature", ssh_key_pair.KeySignature)
}

func resourceNoradSshKeyPairCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	oid := d.Get("organization_id").(string)
	ssh_key_pair_opts := &norad.CreateSshKeyPairOptions{
		SshKeyPairCreationParameters: norad.SshKeyPairCreationParameters{
			Name:        d.Get("name").(string),
			Description: d.Get("description").(string),
			Key:         d.Get("key").(string),
			Username:    d.Get("username").(string),
		},
	}
	ssh_key_pair, _, err := client.SshKeyPairs.CreateSshKeyPair(oid, ssh_key_pair_opts)

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", ssh_key_pair.Id))
	resourceNoradSshKeyPairSetToState(d, ssh_key_pair)
	return nil
}

func resourceNoradSshKeyPairRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	ssh_key_pair, _, err := client.SshKeyPairs.GetSshKeyPair(d.Id())

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", ssh_key_pair.Id))
	resourceNoradSshKeyPairSetToState(d, ssh_key_pair)
	return nil
}

func resourceNoradSshKeyPairUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)

	ssh_key_pair_opts := &norad.UpdateSshKeyPairOptions{
		SshKeyPairUpdateParameters: norad.SshKeyPairUpdateParameters{
			Name:        d.Get("name").(string),
			Description: d.Get("description").(string),
		},
	}

	ssh_key_pair, _, err := client.SshKeyPairs.UpdateSshKeyPair(d.Id(), ssh_key_pair_opts)
	if err != nil {
		return err
	}

	resourceNoradSshKeyPairSetToState(d, ssh_key_pair)
	return nil
}

func resourceNoradSshKeyPairDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	_, err := client.SshKeyPairs.DeleteSshKeyPair(d.Id())
	return err
}
