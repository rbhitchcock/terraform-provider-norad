package norad

import (
	"fmt"
	"github.com/hashicorp/terraform/helper/schema"
	"gitlab.com/norad/go-norad"
)

func resourceNoradSecurityTestRepository() *schema.Resource {
	return &schema.Resource{
		Create: resourceNoradSecurityTestRepositoryCreate,
		Read:   resourceNoradSecurityTestRepositoryRead,
		Update: resourceNoradSecurityTestRepositoryUpdate,
		Delete: resourceNoradSecurityTestRepositoryDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"host": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"password": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"username": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"public": &schema.Schema{
				Type:     schema.TypeBool,
				Required: true,
			},
			"official": &schema.Schema{
				Type:     schema.TypeBool,
				Computed: true,
			},
		},
	}
}

func resourceNoradSecurityTestRepositorySetToState(d *schema.ResourceData, repo *norad.SecurityTestRepository) {
	d.Set("name", repo.Name)
	d.Set("public", repo.Public)
	d.Set("official", repo.Official)
	d.Set("host", repo.Host)
	d.Set("username", repo.Username)
}

func resourceNoradSecurityTestRepositoryCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	repo_opts := &norad.CreateSecurityTestRepositoryOptions{
		SecurityTestRepositoryParameters: norad.SecurityTestRepositoryParameters{
			Name:     d.Get("name").(string),
			Host:     d.Get("host").(string),
			Password: d.Get("password").(string),
			Username: d.Get("username").(string),
			Public:   d.Get("public").(bool),
		},
	}
	new_repo, _, err := client.SecurityTestRepositories.CreateSecurityTestRepository(repo_opts)

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", new_repo.Id))
	resourceNoradSecurityTestRepositorySetToState(d, new_repo)
	return nil
}

func resourceNoradSecurityTestRepositoryRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	repo, _, err := client.SecurityTestRepositories.GetSecurityTestRepository(d.Id())

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", repo.Id))
	resourceNoradSecurityTestRepositorySetToState(d, repo)
	return nil
}

func resourceNoradSecurityTestRepositoryUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)

	repo_opts := &norad.UpdateSecurityTestRepositoryOptions{
		SecurityTestRepositoryParameters: norad.SecurityTestRepositoryParameters{
			Name:     d.Get("name").(string),
			Host:     d.Get("host").(string),
			Password: d.Get("password").(string),
			Username: d.Get("username").(string),
			Public:   d.Get("public").(bool),
		},
	}

	repo, _, err := client.SecurityTestRepositories.UpdateSecurityTestRepository(d.Id(), repo_opts)
	if err != nil {
		return err
	}

	resourceNoradSecurityTestRepositorySetToState(d, repo)
	return nil
}

func resourceNoradSecurityTestRepositoryDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	_, err := client.SecurityTestRepositories.DeleteSecurityTestRepository(d.Id())
	return err
}
