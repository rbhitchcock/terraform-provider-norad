package norad

import (
	"fmt"
	"github.com/hashicorp/terraform/helper/schema"
	"gitlab.com/norad/go-norad"
)

func resourceNoradMachine() *schema.Resource {
	return &schema.Resource{
		Create: resourceNoradMachineCreate,
		Read:   resourceNoradMachineRead,
		Update: resourceNoradMachineUpdate,
		Delete: resourceNoradMachineDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"ip": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"fqdn": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"description": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"organization_id": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
				ForceNew: true,
			},
			"organization_token": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
				ForceNew: true,
			},
		},
	}
}

func resourceNoradMachineSetToState(d *schema.ResourceData, machine *norad.Machine) {
	d.Set("ip", machine.Ip)
	d.Set("fqdn", machine.Fqdn)
	d.Set("description", machine.Description)
	d.Set("name", machine.Name)
	d.Set("organization_id", machine.OrganizationId)
}

func resourceNoradMachineCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	oid := d.Get("organization_id").(string)
	machine_opts := &norad.CreateMachineOptions{
		MachineParameters: norad.MachineParameters{
			Ip:          d.Get("ip").(string),
			Fqdn:        d.Get("fqdn").(string),
			Description: d.Get("description").(string),
			Name:        d.Get("name").(string),
		},
		OrganizationToken: d.Get("organization_token").(string),
	}
	machine, _, err := client.Machines.CreateMachine(oid, machine_opts)

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", machine.Id))
	resourceNoradMachineSetToState(d, machine)
	return nil
}

func resourceNoradMachineRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	machine, _, err := client.Machines.GetMachine(d.Id())

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", machine.Id))
	resourceNoradMachineSetToState(d, machine)
	return nil
}

func resourceNoradMachineUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)

	options := &norad.UpdateMachineOptions{MachineParameters: norad.MachineParameters{}}

	options.MachineParameters.Ip = d.Get("ip").(string)
	options.MachineParameters.Fqdn = d.Get("fqdn").(string)
	options.MachineParameters.Description = d.Get("description").(string)
	options.MachineParameters.Name = d.Get("name").(string)

	machine, _, err := client.Machines.UpdateMachine(d.Id(), options)
	if err != nil {
		return err
	}

	resourceNoradMachineSetToState(d, machine)
	return nil
}

func resourceNoradMachineDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	_, err := client.Machines.DeleteMachine(d.Id())
	return err
}
