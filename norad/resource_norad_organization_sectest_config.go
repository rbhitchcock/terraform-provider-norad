package norad

import (
	"fmt"
	"github.com/hashicorp/terraform/helper/schema"
	"gitlab.com/norad/go-norad"
)

func resourceNoradOrganizationSectestConfig() *schema.Resource {
	return &schema.Resource{
		Create: resourceNoradOrganizationSecurityTestConfigCreate,
		Read:   resourceNoradOrganizationSecurityTestConfigRead,
		Update: resourceNoradOrganizationSecurityTestConfigUpdate,
		Delete: resourceNoradOrganizationSecurityTestConfigDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"values": &schema.Schema{
				Type:      schema.TypeMap,
				Optional:  true,
				Default:   make(map[string]interface{}),
				Sensitive: true,
			},
			"enabled_outside_of_requirement": &schema.Schema{
				Type:     schema.TypeBool,
				Optional: true,
				Default:  true,
			},
			"sectest_id": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
				ForceNew: true,
			},
			"organization_id": &schema.Schema{
				Type:     schema.TypeInt,
				ForceNew: true,
				Required: true,
			},
		},
	}
}

func resourceNoradOrganizationSecurityTestConfigSetToState(d *schema.ResourceData, sectest_config *norad.SecurityTestConfig) {
	d.Set("values", sectest_config.Values)
	d.Set("args", sectest_config.Args)
	d.Set("enabled_outside_of_requirement", sectest_config.EnabledOutsideOfRequirement)
	d.Set("organization_id", sectest_config.OrganizationId)
	d.Set("sectest_id", sectest_config.SecurityTestId)
}

func resourceNoradOrganizationSecurityTestConfigCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	org_id := d.Get("organization_id").(int)
	sectest_config_opts := &norad.CreateSecurityTestConfigOptions{
		SecurityTestConfigCreationParameters: norad.SecurityTestConfigCreationParameters{
			EnabledOutsideOfRequirement: d.Get("enabled_outside_of_requirement").(bool),
			Values:         d.Get("values").(map[string]interface{}),
			SecurityTestId: d.Get("sectest_id").(int),
		},
	}
	security_test_config, _, err := client.SecurityTestConfigs.CreateOrganizationSecurityTestConfig(org_id, sectest_config_opts)

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", security_test_config.Id))
	resourceNoradOrganizationSecurityTestConfigSetToState(d, security_test_config)
	return nil
}

func resourceNoradOrganizationSecurityTestConfigRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	sectest_config, _, err := client.SecurityTestConfigs.GetSecurityTestConfig(d.Id())

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", sectest_config.Id))
	resourceNoradOrganizationSecurityTestConfigSetToState(d, sectest_config)
	return nil
}

func resourceNoradOrganizationSecurityTestConfigUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)

	sectest_config_opts := &norad.UpdateSecurityTestConfigOptions{
		SecurityTestConfigUpdateParameters: norad.SecurityTestConfigUpdateParameters{
			EnabledOutsideOfRequirement: d.Get("enabled_outside_of_requirement").(bool),
			Values: d.Get("values").(map[string]interface{}),
		},
	}

	sectest_config, _, err := client.SecurityTestConfigs.UpdateSecurityTestConfig(d.Id(), sectest_config_opts)
	if err != nil {
		return err
	}

	resourceNoradOrganizationSecurityTestConfigSetToState(d, sectest_config)
	return nil
}

func resourceNoradOrganizationSecurityTestConfigDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	_, err := client.SecurityTestConfigs.DeleteSecurityTestConfig(d.Id())
	return err
}
