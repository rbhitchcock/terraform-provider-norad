package norad

import (
	"github.com/hashicorp/terraform/helper/schema"
	"github.com/hashicorp/terraform/terraform"
)

// Provider returns a terraform.ResourceProvider.
func Provider() terraform.ResourceProvider {

	// The actual provider
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"api_token": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("NORAD_API_TOKEN", nil),
				Description: descriptions["api_token"],
			},
			"base_url": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("NORAD_BASE_URL", "https://norad.cisco.com:8443/v1/"),
				Description: descriptions["base_url"],
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"norad_organization":                resourceNoradOrganization(),
			"norad_machine":                     resourceNoradMachine(),
			"norad_ssh_service":                 resourceNoradSshService(),
			"norad_generic_service":             resourceNoradGenericService(),
			"norad_web_application_service":     resourceNoradWebApplicationService(),
			"norad_security_test_repository":    resourceNoradSecurityTestRepository(),
			"norad_sectest":                     resourceNoradSectest(),
			"norad_organization_sectest_config": resourceNoradOrganizationSectestConfig(),
			"norad_machine_sectest_config":      resourceNoradMachineSectestConfig(),
			"norad_ssh_key_pair":                resourceNoradSshKeyPair(),
			"norad_ssh_key_pair_assignment":     resourceNoradSshKeyPairAssignment(),
		},

		ConfigureFunc: providerConfigure,
	}
}

var descriptions map[string]string

func init() {
	descriptions = map[string]string{
		"api_token": "The API token used to connect to Norad.",
		"base_url":  "The Norad Base API URL",
	}
}

func providerConfigure(d *schema.ResourceData) (interface{}, error) {
	config := Config{
		Token:   d.Get("api_token").(string),
		BaseURL: d.Get("base_url").(string),
	}

	return config.Client()
}
