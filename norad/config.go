package norad

import (
	"gitlab.com/norad/go-norad"
)

type Config struct {
	Token   string
	BaseURL string
}

// Client returns a norad.Client to interact with the configured Norad instance
func (c *Config) Client() (interface{}, error) {
	client := norad.NewClient(nil, c.BaseURL, c.Token, true)
	return client, nil
}
