package norad

import (
	"fmt"
	"github.com/hashicorp/terraform/helper/schema"
	"gitlab.com/norad/go-norad"
)

func resourceNoradWebApplicationService() *schema.Resource {
	return &schema.Resource{
		Create: resourceNoradWebApplicationServiceCreate,
		Read:   resourceNoradWebApplicationServiceRead,
		Update: resourceNoradWebApplicationServiceUpdate,
		Delete: resourceNoradWebApplicationServiceDelete,

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"description": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"port": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"allow_brute_force": &schema.Schema{
				Type:     schema.TypeBool,
				Optional: true,
			},
			"encryption_type": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"machine_id": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
				ForceNew: true,
			},
		},
	}
}

func resourceNoradWebApplicationServiceSetToState(d *schema.ResourceData, web_application *norad.WebApplicationService) {
	d.Set("description", web_application.Description)
	d.Set("name", web_application.Name)
	d.Set("machine_id", web_application.MachineId)
	d.Set("port", web_application.Port)
	d.Set("allow_brute_force", web_application.AllowBruteForce)
	d.Set("encryption_type", web_application.EncryptionType)
}

func resourceNoradWebApplicationServiceCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	mid := d.Get("machine_id").(int)
	web_application_opts := &norad.CreateWebApplicationServiceOptions{
		WebApplicationServiceCreationParameters: norad.WebApplicationServiceCreationParameters{
			Name:            d.Get("name").(string),
			Description:     d.Get("description").(string),
			Port:            d.Get("port").(string),
			AllowBruteForce: d.Get("allow_brute_force").(bool),
			EncryptionType:  d.Get("encryption_type").(string),
		},
	}
	web_application, _, err := client.WebApplicationServices.CreateWebApplicationService(mid, web_application_opts)

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", web_application.Id))
	resourceNoradWebApplicationServiceSetToState(d, web_application)
	return nil
}

func resourceNoradWebApplicationServiceRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	web_application, _, err := client.WebApplicationServices.GetWebApplicationService(d.Id())

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", web_application.Id))
	resourceNoradWebApplicationServiceSetToState(d, web_application)
	return nil
}

func resourceNoradWebApplicationServiceUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)

	options := &norad.UpdateWebApplicationServiceOptions{WebApplicationServiceUpdateParameters: norad.WebApplicationServiceUpdateParameters{}}

	options.WebApplicationServiceUpdateParameters.Description = d.Get("description").(string)
	options.WebApplicationServiceUpdateParameters.Name = d.Get("name").(string)
	options.WebApplicationServiceUpdateParameters.Port = d.Get("port").(string)
	options.WebApplicationServiceUpdateParameters.AllowBruteForce = d.Get("allow_brute_force").(bool)
	options.WebApplicationServiceUpdateParameters.EncryptionType = d.Get("encryption_type").(string)

	web_application, _, err := client.WebApplicationServices.UpdateWebApplicationService(d.Id(), options)
	if err != nil {
		return err
	}

	resourceNoradWebApplicationServiceSetToState(d, web_application)
	return nil
}

func resourceNoradWebApplicationServiceDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	_, err := client.WebApplicationServices.DeleteWebApplicationService(d.Id())
	return err
}
