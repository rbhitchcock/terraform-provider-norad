package norad

import (
	"fmt"
	"github.com/hashicorp/terraform/helper/schema"
	"gitlab.com/norad/go-norad"
)

func resourceNoradSectest() *schema.Resource {
	return &schema.Resource{
		Create: resourceNoradSecurityTestCreate,
		Read:   resourceNoradSecurityTestRead,
		Update: resourceNoradSecurityTestUpdate,
		Delete: resourceNoradSecurityTestDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"category": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"help_url": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"prog_args": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"default_config": &schema.Schema{
				Type:     schema.TypeMap,
				Optional: true,
			},
			"test_types": &schema.Schema{
				Type:     schema.TypeList,
				Required: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"multi_host": &schema.Schema{
				Type:     schema.TypeBool,
				Required: true,
			},
			"configurable": &schema.Schema{
				Type:     schema.TypeBool,
				Required: true,
			},
			"security_test_repository_id": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
		},
	}
}

func resourceNoradSecurityTestSetToState(d *schema.ResourceData, security_test *norad.SecurityTest) {
	d.Set("name", security_test.Name)
	d.Set("category", security_test.Category)
	d.Set("prog_args", security_test.ProgArgs)
	d.Set("default_config", security_test.DefaultConfig)
	d.Set("multi_host", security_test.MultiHost)
	d.Set("test_types", security_test.TestTypes)
	d.Set("configurable", security_test.Configurable)
	d.Set("help_url", security_test.HelpUrl)
	d.Set("security_test_repository_id", security_test.SecurityTestRepositoryId)
}

func resourceNoradSecurityTestCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	rid := d.Get("security_test_repository_id").(string)
	security_test_opts := &norad.CreateSecurityTestOptions{
		SecurityTestParameters: norad.SecurityTestParameters{
			Name:                     d.Get("name").(string),
			Category:                 d.Get("category").(string),
			ProgArgs:                 d.Get("prog_args").(string),
			MultiHost:                d.Get("multi_host").(bool),
			TestTypes:                d.Get("test_types").([]string),
			DefaultConfig:            d.Get("default_config").(map[string]string),
			Configurable:             d.Get("configurable").(bool),
			HelpUrl:                  d.Get("help_url").(string),
			SecurityTestRepositoryId: d.Get("security_test_repository_id").(int),
		},
	}
	security_test, _, err := client.SecurityTests.CreateSecurityTest(rid, security_test_opts)

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", security_test.Id))
	resourceNoradSecurityTestSetToState(d, security_test)
	return nil
}

func resourceNoradSecurityTestRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	security_test, _, err := client.SecurityTests.GetSecurityTest(d.Id())

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", security_test.Id))
	resourceNoradSecurityTestSetToState(d, security_test)
	return nil
}

func resourceNoradSecurityTestUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)

	security_test_opts := &norad.UpdateSecurityTestOptions{
		SecurityTestParameters: norad.SecurityTestParameters{
			Name:                     d.Get("name").(string),
			Category:                 d.Get("category").(string),
			ProgArgs:                 d.Get("prog_args").(string),
			MultiHost:                d.Get("bool").(bool),
			TestTypes:                d.Get("test_types").([]string),
			DefaultConfig:            d.Get("default_config").(map[string]string),
			Configurable:             d.Get("configurable").(bool),
			HelpUrl:                  d.Get("help_url").(string),
			SecurityTestRepositoryId: d.Get("security_test_repository_id").(int),
		},
	}

	security_test, _, err := client.SecurityTests.UpdateSecurityTest(d.Id(), security_test_opts)
	if err != nil {
		return err
	}

	resourceNoradSecurityTestSetToState(d, security_test)
	return nil
}

func resourceNoradSecurityTestDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	_, err := client.SecurityTests.DeleteSecurityTest(d.Id())
	return err
}
