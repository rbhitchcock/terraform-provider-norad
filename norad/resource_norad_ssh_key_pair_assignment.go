package norad

import (
	"fmt"
	"github.com/hashicorp/terraform/helper/schema"
	"gitlab.com/norad/go-norad"
)

func resourceNoradSshKeyPairAssignment() *schema.Resource {
	return &schema.Resource{
		Create: resourceNoradSshKeyPairAssignmentCreate,
		Read:   resourceNoradSshKeyPairAssignmentRead,
		Delete: resourceNoradSshKeyPairAssignmentDelete,

		Schema: map[string]*schema.Schema{
			"machine_id": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
				ForceNew: true,
			},
			"ssh_key_pair_id": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
				ForceNew: true,
			},
		},
	}
}

func resourceNoradSshKeyPairAssignmentSetToState(d *schema.ResourceData, ssh_key_pair_assignment *norad.SshKeyPairAssignment) {
	d.Set("machine_id", ssh_key_pair_assignment.MachineId)
	d.Set("ssh_key_pair_id", ssh_key_pair_assignment.SshKeyPairId)
}

func resourceNoradSshKeyPairAssignmentCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	mid := d.Get("machine_id").(int)
	ssh_key_pair_assignment_opts := &norad.CreateSshKeyPairAssignmentOptions{
		SshKeyPairId: d.Get("ssh_key_pair_id").(int),
	}
	ssh_key_pair_assignment, _, err := client.SshKeyPairAssignments.CreateSshKeyPairAssignment(mid, ssh_key_pair_assignment_opts)

	if err != nil {
		return err
	}

	d.SetId(fmt.Sprintf("%d", ssh_key_pair_assignment.Id))
	resourceNoradSshKeyPairAssignmentSetToState(d, ssh_key_pair_assignment)
	return nil
}

func resourceNoradSshKeyPairAssignmentDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*norad.Client)
	_, err := client.SshKeyPairAssignments.DeleteSshKeyPairAssignment(d.Get("machine_id"))
	return err
}

func resourceNoradSshKeyPairAssignmentRead(d *schema.ResourceData, meta interface{}) error {
	// Currently there is no GET /:id endpoint for Key Pair Assignments
	return nil
}
